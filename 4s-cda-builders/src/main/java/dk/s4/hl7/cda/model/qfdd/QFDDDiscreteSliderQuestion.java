package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.model.CodedValue;

/**
 * Multiple choice question where the user may only provide at maximum one
 * answer. The answer is optional by the use of minimum which may be 0 or 1.
 */
public final class QFDDDiscreteSliderQuestion extends QFDDQuestion {
  private static final Logger logger = LoggerFactory.getLogger(QFDDDiscreteSliderQuestionBuilder.class);
  private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
  private int minimum; // head value

  /**
   * "Effective Java" Builder for constructing QFDDDiscreteSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDDiscreteSliderQuestionBuilder
      extends QFDDQuestion.BaseQFDDQuestionBuilder<QFDDDiscreteSliderQuestion, QFDDDiscreteSliderQuestionBuilder> {

    private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();

    public QFDDDiscreteSliderQuestionBuilder() {
    }

    private int minimum; // head value

    public QFDDDiscreteSliderQuestionBuilder setMinimum(int minimum) {
      this.minimum = minimum;
      return this;
    }

    private void validateMinimum() {
      if (minimum != 0 && minimum != 1) {
        logger.warn("The minimum in the Discrete Slider must be 1 or 0 but was: " + minimum + ". Defaulting to 0");
        minimum = 0;
      }
    }

    public QFDDDiscreteSliderQuestionBuilder addAnswerOption(String code, String codeSystem, String displayName,
        String codeSystemName) {
      answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
      return this;
    }

    @Override
    public QFDDDiscreteSliderQuestion build() {
      validateMinimum();
      return new QFDDDiscreteSliderQuestion(this);
    }

    @Override
    public QFDDDiscreteSliderQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDDiscreteSliderQuestion(QFDDDiscreteSliderQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    answerOptionList = builder.answerOptionList;
  }

  public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
    answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }

  public List<CodedValue> getAnswerOptionList() {
    return answerOptionList;
  }

  public int getMinimum() {
    return minimum;
  }

  public int getMaximum() {
    return 1;
  }
}
