package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdTime {
  private String appointmentStart;
  private String appointmentEnd;
  private String appointmentCron;
  private String appointmentDuration;

  public String getAppointmentStart() {
    return appointmentStart;
  }

  public void setAppointmentStart(String appointmentStart) {
    this.appointmentStart = appointmentStart;
  }

  public String getAppointmentEnd() {
    return appointmentEnd;
  }

  public void setAppointmentEnd(String appointmentEnd) {
    this.appointmentEnd = appointmentEnd;
  }

  public String getAppointmentCron() {
    return appointmentCron;
  }

  public void setAppointmentCron(String appointmentCron) {
    this.appointmentCron = appointmentCron;
  }

  public String getAppointmentDuration() {
    return appointmentDuration;
  }

  public void setAppointmentDuration(String appointmentDuration) {
    this.appointmentDuration = appointmentDuration;
  }

  public boolean isStandardTimeUsed() {
    return !isNullOrEmpty(this.appointmentStart) && !isNullOrEmpty(this.appointmentEnd);
  }

  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.appointmentStart) && isNullOrEmpty(this.appointmentEnd)
        && isNullOrEmpty(this.appointmentCron) && isNullOrEmpty(this.appointmentDuration);
  }

  private boolean isNullOrEmpty(String value) {
    return value == null || value.toLowerCase().equals("null") || value.equals("");
  }
}
