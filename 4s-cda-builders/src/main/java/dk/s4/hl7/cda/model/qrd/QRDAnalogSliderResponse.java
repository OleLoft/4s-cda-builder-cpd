package dk.s4.hl7.cda.model.qrd;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * From specification:
 * The Analog Slider Question Pattern Observation is used to ask a question from
 * the patient in the form of visual analogue scale (VAS). The Analog Slider
 * Question Pattern Observation is used to create an instance that carries the
 * information necessary to construct VAS.
 * 
 * The analog slider is defined by a minimum and maximum range. including the
 * increment.
 * The response holds the users answer by value and type
 * 
 */
public class QRDAnalogSliderResponse extends QRDResponse {
  private String minimum; // head value
  private String maximum; // denominator
  private String increment;
  private String value;
  private String unit;

  /**
   * "Effective Java" Builder for constructing QRDAnalogSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDAnalogSliderResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDAnalogSliderResponse, QRDAnalogSliderResponseBuilder> {

    public QRDAnalogSliderResponseBuilder() {
    }

    private String minimum;
    private String maximum;
    private String increment;
    private String value;
    private String unit;

    public QRDAnalogSliderResponseBuilder setInterval(String minimum, String maximum, String increment) {
      ModelUtil.checkNull(minimum, "Minimum value is mandatory");
      ModelUtil.checkNull(maximum, "Maximum value is mandatory");
      ModelUtil.checkNull(increment, "Increment value is mandatory");
      this.minimum = minimum;
      this.maximum = maximum;
      this.increment = increment;
      return this;
    }

    public QRDAnalogSliderResponseBuilder setValue(String value, String unit) {
      ModelUtil.checkNull(value, "Value is mandatory");
      ModelUtil.checkNull(unit, "Unit type is mandatory");
      this.value = value;
      this.unit = unit;
      return this;
    }

    @Override
    public QRDAnalogSliderResponse build() {
      ModelUtil.checkNull(value, "Please set the value");
      ModelUtil.checkNull(increment, "Please set the interval");
      return new QRDAnalogSliderResponse(this);
    }

    @Override
    public QRDAnalogSliderResponseBuilder getThis() {
      return this;
    }
  }

  private QRDAnalogSliderResponse(QRDAnalogSliderResponseBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    increment = builder.increment;
    value = builder.value;
    unit = builder.unit;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getIncrement() {
    return increment;
  }

  public String getValue() {
    return value;
  }

  public String getUnit() {
    return unit;
  }
}