package dk.s4.hl7.util.xml.hashing;

public class HashableString extends HashableCharSequence {
  private String text;

  public HashableString() {
    this.text = null;
  }

  public HashableString(String text) {
    super(text);
    this.text = text;
  }

  @Override
  public int length() {
    return text.length();
  }

  @Override
  public char charAt(int index) {
    return text.charAt(index);
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    return text.subSequence(start, end);
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(text, obj);
  }

  @Override
  public boolean equalsIgnoreCase(CharSequence text) {
    return super.equalsIgnoreCase(this.text, text);
  }

  @Override
  public String toString() {
    return text;
  }

  public void setText(String text) {
    hasher.reset();
    this.text = text;
    if (text != null) {
      hasher.appendHash(0, text);
    }
  }
}
