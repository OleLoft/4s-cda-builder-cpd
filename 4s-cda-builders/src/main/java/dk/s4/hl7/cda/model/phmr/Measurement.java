package dk.s4.hl7.cda.model.phmr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Comment;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Interval;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.ReferenceRange;

/** The role of a measurement (vital signs, result) in the SimpleClinicalDocument.
 * While you may build measurements using this class directly (see
 * the inner class 'MeasurementBuilder') we advice you to use the
 * convenience methods for defining recurring tele medicine types
 * of measurements found in the NPU class.
 * 
 * Measurement objects translate into an 'Observation' sections in the PHMR.
 * 
 * Design notes:
 * 
 * The present design is somewhat of a compromise, as objects of
 * this class may EITHER represent a numerical measurement like weight
 * (physical quantity in HL7 lingo) OR a reference to some
 * external media like a PNG with a CTG graph (observation media
 * in HL7 lingo). Thus given an instance, you have to test on
 * the getType() method to verify if it represents
 * PHYSICAL_QUANTITY or OBSERVATION_MEDIA. In case of
 * a PHYSICAL_QUANTITY the methods getValue(), getUnit(), etc.
 * have non-null values; in case of OBSERVATION_MEDIA
 * the methods getReference() etc. will return non-null values.
 * 
 * A design based on inheritance will suffer the same
 * type-switching problems (ie. having to put
 * if ( m instance of PhysicalQuantityMeasurement) {
 *   PhysicalQuantityMeasurement pq = (PhysicalQuantityMeasurement) m;
 *   etc.
 * all over the place.
 * 
 * The correct design is to mimic the HL7 structure 
 * 
 * Observation obs = new Observation(timestamp, ...);
 * Entry weight = new PhysicalQuantity(77, "kg", ...);
 * 
 * obs.addEntry(weight);
 * 
 * etc. HOWEVER, as we judge that the only two really
 * used entry types are measurements OR references to
 * media files, we deem this an overgeneralization and make the
 * client code more cumbersome to write. Thus this
 * decision.
 *
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public class Measurement {

  private List<Reference> references;

  /** HL7 status codes for a measurement. */
  public enum Status {
    COMPLETED, // Measurement is valid and completed 
    NULLIFIED // Measurement has been cancelled and is invalid
  }

  /** The types of contents a measurement may have. */
  public enum Type {
    OBSERVATION_MEDIA,
    PHYSICAL_QUANTITY
  }

  /** The Builder for creating instances of a measurement.*/
  public static class MeasurementBuilder {
    // Mandatory fields
    private Date when;
    private Status status;

    // Mandatory fields for PhysicalQuantity (value,unit,code,displayname) or (valueInterval,unit,code,displayname)
    // Note, these are mutually exclusive of ObservationMedia references
    private String value = null;
    private Interval valueInterval = null;
    // The decision to represent what is a double value as a string is
    // a hard one. The reason is formatting - clinicians have different
    // habits as how numbers are represented: it is 170 mm[Hg], not 170.0 mm[Hg].
    // Coding rules is doomed to fail or get exceedingly complex. We avoid it!
    private String unit = null;
    private String code = null;
    private String displayName = null;

    // Mandatory fields for ObservationMedia references
    // Note, these are mutually exclusive to PhysicalQuantity
    private String referenceID = null;
    private String reference = null;

    // Truely optional fields
    private DataInputContext context;

    // Fields with default values, the NPU coding system in Denmark
    private String codeSystemOID = NPU.CODESYSTEM_OID;
    private String codeSystemName = NPU.DISPLAYNAME;

    // Translated coded value fields, should correspond to code, displayName
    private String translatedCode = null;
    private String translatedDisplayName = null;
    private String translatedCodeSystemOID = null;
    private String translatedCodeSystemName = null;

    //The id specific data for the observation
    private ID id;

    // Observation ranges
    private List<ReferenceRange> observationRanges = new ArrayList<ReferenceRange>();

    /** Construct the core measurement.
     * 
     * @param when timestamp of this measurement
     * @param status status of the measurement
     */
    public MeasurementBuilder(Date when, Status status) {
      this.when = when;
      this.status = status;
    }

    public MeasurementBuilder() {
    }

    /** Define the measured value (value, unit, code) that
     * this measurement represents; defaulting to the NPU code system.
     * @param valueAsString the value in string format, e.g. "77.2"
     * @param ucumUnitString the unit in the UCUM system, e.g. UCUM.kg
     * @param code the code defining the type of measurement, e.g. NPU.DRY_BODY_WEIGHT_CODE
     * @param displayname the displayname associated with the code, e.g. NPU.DRY_BODY_WEIGHT_DISPLAYNAME
     * @return builder
     */
    public MeasurementBuilder setPhysicalQuantity(String valueAsString, String ucumUnitString, String code,
        String displayname) {
      this.value = replaceCommaWithDot(valueAsString);
      this.unit = ucumUnitString;
      this.code = code;
      this.displayName = displayname;
      return this;
    }

    public MeasurementBuilder setPhysicalQuantity(String valueAsString, String ucumUnitString, String code,
        String displayname, String codeSystemOID, String codeSystemDisplayName) {
      this.value = replaceCommaWithDot(valueAsString);
      this.unit = ucumUnitString;
      this.code = code;
      this.displayName = displayname;
      this.codeSystemOID = codeSystemOID;
      this.codeSystemName = codeSystemDisplayName;
      return this;
    }

    public MeasurementBuilder setPhysicalQuantity(String valueAsString, String ucumUnitString, CodedValue code) {
      this.value = replaceCommaWithDot(valueAsString);
      this.unit = ucumUnitString;
      this.code = code.getCode();
      this.displayName = code.getDisplayName();
      this.codeSystemOID = code.getCodeSystem();
      this.codeSystemName = code.getCodeSystemName();
      return this;
    }

    /** Define the measured value as an interval (value, unit, code) that
     * this measurement represents; defaulting to the NPU code system.
     * @param valueInterval the value given as interval (for instance an interval where low is not included and unknown
     * and high is included with a value "77.2" corresponding to a overall value <= 77.2.
     * @param ucumUnitString the unit in the UCUM system, e.g. UCUM.kg
     * @param code the code defining the type of measurement, e.g. NPU.DRY_BODY_WEIGHT_CODE
     * @param displayname the displayname associated with the code, e.g. NPU.DRY_BODY_WEIGHT_DISPLAYNAME
     * @return builder
     */
    public MeasurementBuilder setPhysicalQuantity(Interval valueInterval, String ucumUnitString, CodedValue code) {
      this.valueInterval = replaceCommaWithDot(valueInterval);
      this.unit = ucumUnitString;
      this.code = code.getCode();
      this.displayName = code.getDisplayName();
      this.codeSystemOID = code.getCodeSystem();
      this.codeSystemName = code.getCodeSystemName();
      return this;
    }

    /**
     * Defines a translation corresponding to the code, display name, and code system set with
     * {@link #setPhysicalQuantity()}.
     * @param translatedCode the translated code
     * @param translatedDisplayName the translated display name
     * @param translatedCodeSystemOID the translated code system
     * @param translatedCodeSystemName the translated code system name
     * @return builder
     */
    public MeasurementBuilder setTranlated(String translatedCode, String translatedDisplayName,
        String translatedCodeSystemOID, String translatedCodeSystemName) {
      this.translatedCode = translatedCode;
      this.translatedDisplayName = translatedDisplayName;
      this.translatedCodeSystemOID = translatedCodeSystemOID;
      this.translatedCodeSystemName = translatedCodeSystemName;
      return this;
    }

    private Interval replaceCommaWithDot(Interval valueInterval) {
      Interval.IntervalBuilder builder = new Interval.IntervalBuilder();
      if (valueInterval.isLowValueUnknown()) {
        builder.setLowValueUnknown();
      } else {
        builder.setLowValue(replaceCommaWithDot(valueInterval.getLowValue()), valueInterval.isLowValueInclusive());
      }
      if (valueInterval.isHighValueUnknown()) {
        builder.setHighValueUnknown();
      } else {
        builder.setHighValue(replaceCommaWithDot(valueInterval.getHighValue()), valueInterval.isHighValueInclusive());
      }
      return builder.build();
    }

    private String replaceCommaWithDot(String text) {
      if (text != null) {
        return text.replace(',', '.');
      }
      return text;
    }

    public MeasurementBuilder setDate(Date date) {
      this.when = date;
      return this;
    }

    public MeasurementBuilder setStatus(Status status) {
      this.status = status;
      return this;
    }

    /** Define the context of this measurement.
     * 
     * @param context a context object that defines who made the
     * measurement and by what means it was provided.
     * @return builder
     */
    public MeasurementBuilder setContext(DataInputContext context) {
      this.context = context;
      return this;
    }

    /** In case you need to define measurements in other code systems besides
     * the NPU system, use this method to define the code system used.
     * @param codesystemOid HL7 OID of the code system to use, e.g. LOINCs OID.
     * @param displayNameOfCodeSystem the displayname for the code system.
     * @return builder
     */
    public MeasurementBuilder useAlternativeCodingSystem(String codesystemOid, String displayNameOfCodeSystem) {
      this.codeSystemOID = codesystemOid;
      this.codeSystemName = displayNameOfCodeSystem;
      return this;
    }

    /** Define a reference to a media containing the measurement, like e.g.
     * a PNG with a CTG graph.
     * @param referenceId the unique ID of the media
     * @param actualReference the actual media reference, e.g. an URL
     * @return builder
     */
    public MeasurementBuilder setObservationMediaReference(String referenceId, String actualReference) {
      this.referenceID = referenceId;
      this.reference = actualReference;
      return this;
    }

    public MeasurementBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    public MeasurementBuilder addReferenceRange(ReferenceRange range) {
      observationRanges.add(range);
      return this;
    }

    /** Build the measurement based upon the given parameters.
     * 
     * @return the final measurement.
     */
    public Measurement build() {
      validateWellformednessAndThrowExceptionIfNot();
      return new Measurement(this);
    }

    /** Required validations; non essential ones should instead by done
     * by validation builders for the SimpleClinicalDocument.
     */
    private void validateWellformednessAndThrowExceptionIfNot() {
      // A measurement cannot both be a physical quantity AND
      // an observation media at the same time
      if ((value != null || valueInterval != null) && referenceID != null) {
        throw new RuntimeException(
            "A measurement must EITHER represent a physical quantity OR an observation media, NOT both!");
      }
      if (value == null && valueInterval == null && referenceID == null) {
        throw new RuntimeException(
            "A measurement MUST include either a physical quantity or an observation media. You have not set any of them.");
      }
      if (value != null && valueInterval != null) {
        throw new RuntimeException(
            "A measurement representing a physical quantity must EITHER be given by single value or interval value, NOT both!");
      }
    }
  }

  /** Private constructor, used by the Builder */
  private Measurement(MeasurementBuilder builder) {
    this.timestamp = builder.when;
    this.completed = builder.status;
    this.value = builder.value;
    this.valueInterval = builder.valueInterval;
    this.unitString = builder.unit;
    this.code = builder.code;
    this.displayName = builder.displayName;
    this.codeSystemOID = builder.codeSystemOID;
    this.codeSystemDisplayName = builder.codeSystemName;
    this.translatedCode = builder.translatedCode;
    this.translatedDisplayName = builder.translatedDisplayName;
    this.translatedCodeSystemOID = builder.translatedCodeSystemOID;
    this.translatedCodeSystemName = builder.translatedCodeSystemName;
    this.context = builder.context;
    this.referenceID = builder.referenceID;
    this.reference = builder.reference;
    this.references = new ArrayList<Reference>();
    this.id = builder.id;
    this.observationRanges = builder.observationRanges;

    // Infer the type
    if (this.referenceID != null) {
      type = Type.OBSERVATION_MEDIA;
    } else if (this.value != null || this.valueInterval != null) {
      type = Type.PHYSICAL_QUANTITY;
    } else {
      // Should not occur, has been detected in the build() method
    }
  }

  private Date timestamp;
  private Status completed;
  private DataInputContext context;
  private String value;
  private Interval valueInterval;
  private String unitString;
  private String code;
  private String displayName;
  private String codeSystemOID;
  private String codeSystemDisplayName;
  private String translatedCode;
  private String translatedDisplayName;
  private String translatedCodeSystemOID;
  private String translatedCodeSystemName;
  private String referenceID;
  private String reference;
  private Type type;
  private Comment comment;
  private ID id;
  private List<ReferenceRange> observationRanges;

  /** Get the ID
   * @return  the id.
   */
  public final ID getId() {
    return id;
  }

  /** Gets the value when it is defined as a single value.
   * @return The value. When null is returned, the method {@link getValueInterval()} should be used.
   */
  public String getValue() {
    return value;
  }

  /** Get the value interval when defined as such.
   * @return The value interval. When null is returned, the method {@link getValue()} should be used.
   */
  public Interval getValueInterval() {
    return valueInterval;
  }

  /** Get the unit.
   * @return The Unit.
   */
  public String getUnit() {
    return unitString;
  }

  /** Get the code.
   * 
   * @return The code.
   */
  public String getCode() {
    return code;
  }

  /** Get the display name.
   * @return The display name.
   */
  public String getDisplayName() {
    return displayName;
  }

  /** Get the code system OID.
   * @return The code system OID.
   */
  public final String getCodeSystem() {
    return codeSystemOID;
  }

  /** Get the code system name.
   * @return The code system name.
   */
  public final String getCodeSystemName() {
    return codeSystemDisplayName;
  }

  /**
   * Gets the possible translation of {@link #getCode()}
   * @return The translated code
   */
  public String getTranslatedCode() {
    return translatedCode;
  }

  /**
   * Gets the possible displayName for the translation of {@link #getCode()}
   * @return The translated display name
   */
  public String getTranslatedDisplayName() {
    return translatedDisplayName;
  }

  /**
   * Gets the possible code system for the translation of {@link #getCode()}
   * @return The translated code system OID 
   */
  public String getTranslatedCodeSystem() {
    return translatedCodeSystemOID;
  }

  /**
   * Gets the possible code system name for the translation of {@link #getCode()}
   * @return The translated code system name 
   */
  public String getTranslatedCodeSystemName() {
    return translatedCodeSystemName;
  }

  /** Get the measurement time.
   * @return The measurement time.
   */
  public final Date getTimestamp() {
    return timestamp;
  }

  /** Get the status of this measurement.
   * @return the status of the measurement
   */
  public final Status getStatus() {
    return completed;
  }

  /** Set whether the measurement is completed.
   * @param completed Boolean stating whether measurement is completed.
   */
  public final void setStatus(Status completed) {
    this.completed = completed;
  }

  /** Get the context.
   * @return The context.
   */
  public final DataInputContext getDataInputContext() {
    return context;
  }

  /** Set the context.
   * @param context The context.
   */
  public final void setDataInputContext(DataInputContext context) {
    this.context = context;
  }

  /** Get the comment.
   * @return The comment.
   */
  public final Comment getComment() {
    return comment;
  }

  /** Set the comment.
   * @param comment The comment.
   */
  public final void setComment(Comment comment) {
    this.comment = comment;
  }

  /** Get whether there is a comment.
   * @return Boolean stating whether there is a comment.
   */
  public final boolean hasComment() {
    return this.comment != null;
  }

  /** Get the reference id in case this measurement
   * represents an URL to a media file (observationMedia
   * in HL7 lingo).
   * @return The id of the reference
   */
  public String getReferenceId() {
    return referenceID;
  }

  /** Get the reference in case this measurement
   * represents an URL to a media file (observationMedia
   * in HL7 lingo).
   * @return The reference.
   */
  public String getReference() {
    return reference;
  }

  /** Get the type of this measurement.
   * Based upon the type of the measurement,
   * some methods will always return null.
   * 
   * @return the type
   */
  public Type getType() {
    return type;
  }

  public void addReference(Reference reference) {
    references.add(reference);
  }

  public List<Reference> getReferences() {
    return references;
  }

  public void addObservationRange(ReferenceRange referenceRange) {
    observationRanges.add(referenceRange);
  }

  public List<ReferenceRange> getObservationRanges() {
    return observationRanges;
  }

  /** Return string representation of
   * this.
   * @return string representation
   */
  @Override
  public String toString() {
    String value = "Measurement: ";
    if (getType() == Type.PHYSICAL_QUANTITY) {
      value += getValueOrValueIntervalAsString() + " " + getUnit() + " (" + getDisplayName() + ")";
    } else {
      value += getReference() + " ";
    }
    if (getDataInputContext() != null) {
      DataInputContext context = getDataInputContext();
      value += " [" + context.getMeasurementPerformerCode() + "," + context.getMeasurementProvisionMethodCode() + "]";
    }
    return value;
  }

  private String getValueOrValueIntervalAsString() {
    if (getValue() != null) {
      return getValue();
    } else {
      Interval interval = getValueInterval();
      StringBuilder sb = new StringBuilder();
      if (interval.isLowValueUnknown()) {
        sb.append("]UNKNOWN;");
      } else {
        sb.append(interval.isLowValueInclusive() ? "[" : "]");
        sb.append(interval.getLowValue());
        sb.append(";");
      }
      if (interval.isHighValueUnknown()) {
        sb.append("UNKNOWN[");
      } else {
        sb.append(interval.getHighValue());
        sb.append(interval.isHighValueInclusive() ? "]" : "[");
      }
      return sb.toString();
    }

  }
}
