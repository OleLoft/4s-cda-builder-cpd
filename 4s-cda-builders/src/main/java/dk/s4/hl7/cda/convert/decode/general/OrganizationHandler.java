package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class OrganizationHandler extends BaseXmlHandler {
  private IdHandler idhandler;
  private AddressHandler addressHandler;
  private TelecomHandler telecomHandler;
  private String organizationName;

  public OrganizationHandler(String organizationBasePath, String xpathToOrganizationElementName) {
    addPath(organizationBasePath + xpathToOrganizationElementName);
    idhandler = new IdHandler(organizationBasePath);
    telecomHandler = new TelecomHandler(organizationBasePath);
    addressHandler = new AddressHandler(organizationBasePath, Use.WorkPlace);
  }

  public OrganizationHandler(String organizationBasePath) {
    this(organizationBasePath, "/name");
  }

  public OrganizationIdentity getOrganization() {
    ID organizationId = idhandler.getId();
    if (organizationId != null) {
      return new OrganizationBuilder()
          .setID(organizationId)
          .setAddress(addressHandler.getAddress())
          .setName(organizationName)
          .addTelecoms(telecomHandler.getTelecoms())
          .build();
    }
    return new OrganizationBuilder()
        .setAddress(addressHandler.getAddress())
        .setName(organizationName)
        .addTelecoms(telecomHandler.getTelecoms())
        .build();
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "name")) {
      organizationName = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idhandler.addHandlerToMap(xmlMapping);
    addressHandler.addHandlerToMap(xmlMapping);
    telecomHandler.addHandlerToMap(xmlMapping);
  }

  public void clear() {
    this.idhandler.clear();
    this.addressHandler.clear();
    this.telecomHandler.clear();
    this.organizationName = null;

  }
}
