package dk.s4.hl7.cda.download;

public class DocumentEntry {
  private String homeCommunityId;
  private String repositoryUniqueId;
  private String documentUniqueId;

  public DocumentEntry(String homeCommunityId, String repositoryUniqueId, String documentUniqueId) {
    super();
    this.homeCommunityId = homeCommunityId;
    this.repositoryUniqueId = repositoryUniqueId;
    this.documentUniqueId = documentUniqueId;
  }

  public String getHomeCommunityId() {
    return homeCommunityId;
  }

  public String getRepositoryUniqueId() {
    return repositoryUniqueId;
  }

  public String getDocumentUniqueId() {
    return documentUniqueId;
  }
}
