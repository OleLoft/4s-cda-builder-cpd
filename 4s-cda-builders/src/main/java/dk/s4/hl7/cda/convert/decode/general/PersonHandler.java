package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class PersonHandler extends BaseXmlHandler {
  private String prefixName;
  private List<String> givenNames;
  private String familyName;
  private Gender gender;
  private Date birthTime;
  private String personElementName;
  private boolean hasPerson;

  public PersonHandler(String personParentXPath, String personElementName) {
    this.personElementName = personElementName;
    String personBaseXPath = personParentXPath + "/" + personElementName;
    this.givenNames = new ArrayList<String>();
    addPath(personBaseXPath);
    addPath(personBaseXPath + "/name/prefix");
    addPath(personBaseXPath + "/name/given");
    addPath(personBaseXPath + "/name/family");
    addPath(personBaseXPath + "/administrativeGenderCode");
    addPath(personBaseXPath + "/birthTime");
    hasPerson = false;
  }

  public void addPatientInformation(PatientBuilder patientBuilder) {
    if (hasPerson) {
      for (String givenName : givenNames) {
        patientBuilder.addGivenName(givenName);
      }
      patientBuilder.setPrefix(prefixName);
      patientBuilder.addFamilyName(familyName);
      patientBuilder.setBirthTime(birthTime);
      patientBuilder.setGender(gender);
    }
  }

  public void addPersonInformation(PersonBuilder personBuilder) {
    if (hasPerson) {
      for (String givenName : givenNames) {
        personBuilder.addGivenName(givenName);
      }
      personBuilder.setPrefix(prefixName);
      personBuilder.addFamilyName(familyName);
    }
  }

  public PersonIdentity getPerson() {
    if (hasPerson) {
      PersonBuilder personBuilder = new PersonBuilder();
      addPersonInformation(personBuilder);
      return personBuilder.build();
    }
    return null;
  }

  public Patient getPatient() {
    if (hasPerson) {
      PatientBuilder patientBuilder = new PatientBuilder();
      addPatientInformation(patientBuilder);
      return patientBuilder.build();
    }
    return null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), personElementName)) {
      hasPerson = true;
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "given")) {
      givenNames.add(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "prefix")) {
      prefixName = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "family")) {
      familyName = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "administrativeGenderCode",
        "code")) {
      gender = ConvertXmlUtil.getGender(xmlElement.getAttributeValue("code"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "birthTime", "value")) {
      birthTime = ConvertXmlUtil.getPersonBirthday(xmlElement.getAttributeValue("value"));
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public void clear() {
    this.prefixName = null;
    this.givenNames = new ArrayList<String>();
    this.familyName = null;
    this.gender = null;
    this.birthTime = null;
    this.hasPerson = false;
  }
}
