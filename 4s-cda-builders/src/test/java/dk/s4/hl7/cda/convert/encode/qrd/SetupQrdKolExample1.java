package dk.s4.hl7.cda.convert.encode.qrd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Helper methods to create SimpleClinicalDocuments that match those of MedCom's
 * Qrd example 1.
 *
 * @author Frank Jacobsen, Systematic
 *
 */
public class SetupQrdKolExample1 {

  /** Define a CDA for the Medcom kol example 1. */
  public static QRDDocument defineAsCDA() {
    QRDDocument cda = defineAsQRDKolExample();
    return cda;
  }

  /** Define a CDA for the Medcom kol example */
  public static QRDDocument defineAsQRDKolExample() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2016, 5, 9, 14, 45, 10);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QRD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("edb802b0-2e36-11e6-bdf4-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();
    QRDDocument qrdDocument = new QRDDocument(idHeader);
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");

    // 1.1 Populate with time and version info
    qrdDocument.setDocumentVersion("2358344", 1);
    qrdDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("310541000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qrdDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(nancy.getAddress())
        .setId(nancy.getId())
        .setTelecomList(nancy.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(nancy)
        .build());

    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qrdDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    List<Participant> informationRecipients = new ArrayList<Participant>();
    informationRecipients.add(new ParticipantBuilder()
        .setTelecomList(custodianOrganization.getTelecomList())
        .setId(new IDBuilder()
            .setAuthorityName("Yderregistret To-be-changed")
            .setExtension("310541000016007")
            .setRoot("2.16.840.1.113883.3.4208.100.3")
            .build())
        .setPersonIdentity(nancy)
        .setOrganizationIdentity(custodianOrganization)
        .build());
    qrdDocument.setInformationRecipients(informationRecipients);

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);

    // Set questionnaire type
    qrdDocument.setQuestionnaireType(
        new CodedValue("KCQCOP-10", "1.2.208.999.9.9", "Kansas City COPD Questionnaire", "PRO Spørgeskematyper"));

    // Set introduction section
    qrdDocument.addSection(new Section<QRDResponse>("Vejledning",
        "I forbindelse med dine netop udførte målinger skal vi bede dig besvare følgende spørgsmål.", "da-DK"));

    ID id = new ID.IDBuilder().setRoot("1.2.208.184.100.2").setExtension("ob1").build();

    // Add five answers in individual sections
    // Question 1
    Section<QRDResponse> qRDSection = new Section<QRDResponse>("Spørgsmål 1.", "Spørgsmål 1.");
    CodedValue codeValue = new CodedValue("q1", "1.2.208.184.100.2",
        "Har du taget antibiotika siden sidste måling? (Ja/Nej)", "MedCom prompt table");
    QRDDiscreteSliderResponse qRDResponse = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Har du taget antibiotika siden sidste måling? (Ja/Nej)")
        .setAnswer("A1", "1.2.208.184.100.2", "Ja", "MedCom prompt table")
        .build();
    qRDSection.addQuestionnaireEntity(qRDResponse);
    qrdDocument.addSection(qRDSection);

    // Question 1a
    qRDSection = new Section<QRDResponse>("Spørgsmål 1a.", "Spørgsmål 1a.");
    CodedValue codeValue1 = new CodedValue("q1a", "1.2.208.184.100.2",
        "Var brug af antibiotika pga. forværring i KOL? (Ja/Nej)", "MedCom prompt table");
    QRDDiscreteSliderResponse qRDResponse1 = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue1)
        .setId(id)
        .setQuestion("Var brug af antibiotika pga. forværring i KOL? (Ja/Nej)")
        .setAnswer("A2", "1.2.208.184.100.2", "Nej", "MedCom prompt table")
        .build();
    qRDSection.addQuestionnaireEntity(qRDResponse1);
    qrdDocument.addSection(qRDSection);

    // Question 2
    qRDSection = new Section<QRDResponse>("Spørgsmål 2.", "Spørgsmål 2.");
    CodedValue codeValue2 = new CodedValue("q2", "1.2.208.184.100.2", "Har du mere åndenød? (Ja/Nej)",
        "MedCom prompt table");
    QRDDiscreteSliderResponse qRDResponse2 = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue2)
        .setId(id)
        .setQuestion("Har du mere åndenød? (Ja/Nej)")
        .setAnswer("A1", "1.2.208.184.100.2", "Ja", "MedCom prompt table")
        .build();
    qRDSection.addQuestionnaireEntity(qRDResponse2);
    qrdDocument.addSection(qRDSection);

    // Question 3
    qRDSection = new Section<QRDResponse>("Spørgsmål 3.", "Spørgsmål 3.");
    CodedValue codeValue3 = new CodedValue("q3", "1.2.208.184.100.2", "Har du mere hoste? (Ja/Nej)",
        "MedCom prompt table");
    QRDDiscreteSliderResponse qRDResponse3 = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue3)
        .setId(id)
        .setQuestion("Har du mere hoste? (Ja/Nej)")
        .setAnswer("A1", "1.2.208.184.100.2", "Ja", "MedCom prompt table")
        .build();
    qRDSection.addQuestionnaireEntity(qRDResponse3);
    qrdDocument.addSection(qRDSection);

    // Question4
    qRDSection = new Section<QRDResponse>("Spørgsmål 4.", "Spørgsmål 4.");
    CodedValue codeValue4 = new CodedValue("q4", "1.2.208.184.100.2", "Har du mere åndenød hoste eller slim? (Ja/Nej)",
        "MedCom prompt table");
    QRDDiscreteSliderResponse qRDResponse4 = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue4)
        .setId(id)
        .setQuestion("Har du mere åndenød hoste eller slim? (Ja/Nej)")
        .setAnswer("A1", "1.2.208.184.100.2", "Ja", "MedCom prompt table")
        .build();
    qRDSection.addQuestionnaireEntity(qRDResponse4);
    qrdDocument.addSection(qRDSection);

    return qrdDocument;
  }
}
