package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.convert.v11.APDV11XmlConverter;
import dk.s4.hl7.cda.convert.v11.XmlAPDV11Converter;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to encode and decode Appointment from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 */
public class APDXmlCodec implements Codec<AppointmentDocument, String>, AppendableSerializer<AppointmentDocument>,
    ReaderSerializer<AppointmentDocument> {

  public enum Version {
    V10,
    V11;
  }

  public static final Version DEFAULT_VERSION = Version.V11;
  private final Version version;

  private APDXmlConverter apdXmlConverter;
  private XmlAPDConverter xmlApdConverter;

  public APDXmlCodec() {
    this(new XmlPrettyPrinter());
  }

  public APDXmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this(DEFAULT_VERSION, xmlPrettyPrinter);
  }

  public APDXmlCodec(Version version) {
    this(version, new XmlPrettyPrinter());
  }

  public APDXmlCodec(Version version, XmlPrettyPrinter xmlPrettyPrinter) {
    this.version = version;
    switch (version) {
    case V10:
      this.apdXmlConverter = new APDXmlConverter(xmlPrettyPrinter);
      this.xmlApdConverter = new XmlAPDConverter();
      break;
    case V11:
    default:
      this.apdXmlConverter = new APDV11XmlConverter(xmlPrettyPrinter);
      this.xmlApdConverter = new XmlAPDV11Converter();
      break;
    }
  }

  public Version getVersion() {
    return version;
  }

  public String encode(AppointmentDocument source) {
    return apdXmlConverter.convert(source);
  }

  public AppointmentDocument decode(String source) {
    return xmlApdConverter.convert(source);
  }

  @Override
  public void serialize(AppointmentDocument source, Appendable appendable) {
    if (appendable == null) {
      throw new NullPointerException("Target appendable is null");
    }
    apdXmlConverter.serialize(source, appendable);
  }

  @Override
  public AppointmentDocument deserialize(Reader source) {
    return xmlApdConverter.deserialize(source);
  }
}
