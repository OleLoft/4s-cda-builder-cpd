package dk.s4.hl7.util.xml;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.util.xml.hashing.HashableCharSequence;
import dk.s4.hl7.util.xml.hashing.HashableString;

public class XmlMapping {
  private static final Logger logger = LoggerFactory.getLogger(XmlMapping.class);
  private Map<HashableCharSequence, XmlHandler> handlerMap;

  public XmlMapping() {
    this.handlerMap = new HashMap<HashableCharSequence, XmlHandler>(128);
  }

  public void add(String xpath, XmlHandler xmlHandler) {
    String lowerXPath = xpath.toLowerCase();
    if (handlerMap.containsKey(lowerXPath)) {
      logger.error("Key already exists in XmlMapping: " + lowerXPath);
    }
    handlerMap.put(new HashableString(lowerXPath), xmlHandler);
  }

  public void remove(String xpath) {
    handlerMap.remove(new HashableString(xpath.toLowerCase()));
  }

  public XmlHandler get(HashableCharSequence xpath) {
    return handlerMap.get(xpath);
  }
}