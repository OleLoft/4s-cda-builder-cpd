package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ResponseReferenceRangePatternHandler extends BaseXmlHandler {
  public static final String OBSERVATION_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/referenceRange";
  private String minimum;
  private String maximum;
  private String increment;
  private IntervalType intervalType;
  private String templateId;

  public ResponseReferenceRangePatternHandler() {
    addPath(OBSERVATION_BASE + "/templateId");
    addPath(OBSERVATION_BASE + "/observationRange/value");
    addPath(OBSERVATION_BASE + "/observationRange/value/low");
    addPath(OBSERVATION_BASE + "/observationRange/value/high");
    addPath(OBSERVATION_BASE + "/observationRange/value/head");
    addPath(OBSERVATION_BASE + "/observationRange/value/increment");
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public IntervalType getIntervalType() {
    return intervalType;
  }

  public boolean hasValues() {
    return templateId != null || increment != null;
  }

  public String getIncrement() {
    return increment;
  }

  public void clear() {
    templateId = null;
    minimum = null;
    maximum = null;
    intervalType = null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      String tempTemplateId = xmlElement.getAttributeValue("root");
      if (tempTemplateId.equalsIgnoreCase(HL7.RESPONSE_REFERENCE_RANGE_PATTERN_OID)) {
        templateId = tempTemplateId;
      } else {
        templateId = null;
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type",
        "denominator")) {
      maximum = xmlElement.getAttributeValue("denominator");
      intervalType = IntervalType.findMatchingIntervalType(xmlElement.getAttributeValue("type"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      intervalType = IntervalType.findMatchingIntervalType(xmlElement.getAttributeValue("type"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "head", "value")) {
      minimum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "increment", "value")) {
      increment = xmlElement.getAttributeValue("value");
    }
  }

  public boolean isNumericResponse() {
    return templateId != null && increment == null;
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }
}
