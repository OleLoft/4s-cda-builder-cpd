package dk.s4.hl7.util;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.hl7.v3.POCDMT000040ClinicalDocument;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public class PerfTest {
  private static volatile Object lock = new Object();
  private static volatile JAXBContext jaxbContext;
  private static final int COUNT = 500;

  private static JAXBContext getOrCreateJAXbContext() {
    try {
      if (jaxbContext == null) {
        synchronized (lock) {
          if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(POCDMT000040ClinicalDocument.class);
          }
        }
      }

      return jaxbContext;
    } catch (JAXBException e1) {
      throw new RuntimeException(e1.getMessage(), e1);
    }
  }

  @BeforeClass
  public static void before() {
    Assume.assumeNotNull(System.getProperty("perf_test"));
  }

  @Test
  public void phmrCodec() throws Exception {
    PHMRXmlCodec codec = new PHMRXmlCodec();
    String phmr = FileUtil.getData(this.getClass(), "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
    PHMRDocument document;
    long duration = 0;
    for (int i = 0; i < 10; i++) {
      document = codec.decode(phmr);
      document.hashCode();
    }
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      document = codec.decode(phmr);
      long stop = System.nanoTime();
      document.hashCode();
      duration += stop - start;
    }
    // TODO: What are we testing here? Think we need to decide on some values to
    // check result against!
    System.out.println("PHMR duration: " + duration / 1000000);
  }

  @Test
  public void JAXBTest() throws Exception {
    String phmr = FileUtil.getData(this.getClass(), "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
    long duration = 0;
    getOrCreateJAXbContext().createUnmarshaller();

    for (int i = 0; i < 10; i++) {
      Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
      XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(FileUtil.getReader(phmr));
      POCDMT000040ClinicalDocument obj = unmarshaller
          .unmarshal(xmlStreamReader, POCDMT000040ClinicalDocument.class)
          .getValue();
      obj.hashCode();
    }
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
      XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(FileUtil.getReader(phmr));
      POCDMT000040ClinicalDocument obj = unmarshaller
          .unmarshal(xmlStreamReader, POCDMT000040ClinicalDocument.class)
          .getValue();
      long stop = System.nanoTime();
      obj.hashCode();
      duration += stop - start;
    }
    // TODO: What are we testing here? Think we need to decide on some values to
    // check result against!
    System.out.println("JAXB duration: " + duration / 1000000);
  }

  @Test
  public void writePhmrCodec() throws Exception {
    PHMRXmlCodec codec = new PHMRXmlCodec(null);
    String phmr = FileUtil.getData(this.getClass(), "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
    long duration = 0;
    PHMRDocument phmrDocument = codec.decode(phmr);
    StringWriter writer = new StringWriter(58000);
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      codec.serialize(phmrDocument, writer);
      long stop = System.nanoTime();
      writer.getBuffer().setLength(0);
      duration += stop - start;
    }
    // TODO: What are we testing here? Think we need to decide on some values to
    // check result against!
    System.out.println("PHMR write duration: " + duration / 1000000);
  }

  @Test
  public void writeJAXBTest() throws Exception {
    String phmr = FileUtil.getData(this.getClass(), "phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
    long duration = 0;
    getOrCreateJAXbContext().createUnmarshaller();
    Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
    XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(FileUtil.getReader(phmr));
    JAXBElement<POCDMT000040ClinicalDocument> obj = unmarshaller.unmarshal(xmlStreamReader,
        POCDMT000040ClinicalDocument.class);
    StringWriter writer = new StringWriter(8096);
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      Marshaller marshaller = getOrCreateJAXbContext().createMarshaller();
      marshaller.marshal(obj, writer);
      long stop = System.nanoTime();
      writer.getBuffer().setLength(0);
      duration += stop - start;
    }
    // TODO: What are we testing here? Think we need to decide on some values to
    // check result against!
    System.out.println("JAXB write duration: " + duration / 1000000);
  }
}
