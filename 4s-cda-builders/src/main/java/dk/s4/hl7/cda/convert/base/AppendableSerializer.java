package dk.s4.hl7.cda.convert.base;

/**
 * Interface for converters to support writing to an Appendable object
 */
public interface AppendableSerializer<S> {
  void serialize(S source, Appendable target);
}
