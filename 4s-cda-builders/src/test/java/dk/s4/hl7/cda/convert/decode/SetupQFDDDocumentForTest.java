package dk.s4.hl7.cda.convert.decode;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion.QFDDAnalogSliderQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.GroupType;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.QFDDPreconditionGroupBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupQFDDDocumentForTest {
  public static QFDDDocument defineAsCDA() {
    QFDDDocument qfddDocument = defineQFDDDocumentHeaderInformation();
    addIntroduction(qfddDocument);
    addAnalogSliderSlider(qfddDocument);
    addDiscreteSlider(qfddDocument);
    addMultipleChoice(qfddDocument);
    addNumeric(qfddDocument);
    addText(qfddDocument);
    addQuestionWithComplexPreconditions(qfddDocument);
    addCopyRight(qfddDocument);
    addFinalComment(qfddDocument);
    return qfddDocument;
  }

  private static void addIntroduction(QFDDDocument qfddDocument) {
    qfddDocument.getSections().add(0,
        new Section<QFDDQuestion>(new SectionInformation("introduction", "introduction")));
  }

  /** Define a CDA for the Medcom DiscreteSlider example */
  private static QFDDDocument defineQFDDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();
    QFDDDocument qfddDocument = new QFDDDocument(idHeader);
    qfddDocument.setTitle("KOL spørgeskema");
    qfddDocument.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    qfddDocument.setDocumentVersion("2358344", 1);
    qfddDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qfddDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qfddDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qfddDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qfddDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qfddDocument.setDocumentationTimeInterval(from, to);

    addDiscreteSlider(qfddDocument);

    addMultipleChoice(qfddDocument);

    return qfddDocument;
  }

  private static void addAnalogSliderSlider(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("value1", "value2", "value3", "value4");

    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDAnalogSliderQuestion qfddAnalogSliderQuestion = new QFDDAnalogSliderQuestionBuilder()
        .setInterval("1", "100", "1")
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Question")
        .build();

    section.addQuestionnaireEntity(qfddAnalogSliderQuestion);

    QFDDAnalogSliderQuestion qfddAnalogSliderQuestion1 = new QFDDAnalogSliderQuestionBuilder()
        .setInterval("1", "100", "1")
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Question")
        .build();

    section.addQuestionnaireEntity(qfddAnalogSliderQuestion1);

    qfddDocument.addSection(section);
  }

  private static void addDiscreteSlider(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue1 = new CodedValue("q5", "some-code", "some-display-name", "some-codeSystem-name");

    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDDiscreteSliderQuestion qfddDiscreteSliderQuestion = new QFDDDiscreteSliderQuestion.QFDDDiscreteSliderQuestionBuilder()
        .setMinimum(1)
        .setCodeValue(codeValue1)
        .setId(id)
        .setQuestion("Question")
        .build();

    qfddDiscreteSliderQuestion.addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited",
        "Some-CodeSystem-Name");
    qfddDiscreteSliderQuestion.addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited",
        "Some-CodeSystem-Name");
    qfddDiscreteSliderQuestion.addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited",
        "Some-CodeSystem-Name");

    section.addQuestionnaireEntity(qfddDiscreteSliderQuestion);

    qfddDocument.addSection(section);
  }

  private static void addMultipleChoice(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("q4", "1.2.208.184.100.2", "displayName", "codeSystemName");

    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = new QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Har du mere åndenød hoste eller slim? (Ja/Nej)")
        .build();

    qfddMultipleChoiceQuestion.addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited",
        "Some-CodeSystem-Name");
    qfddMultipleChoiceQuestion.addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited",
        "Some-CodeSystem-Name");
    qfddMultipleChoiceQuestion.addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited",
        "Some-CodeSystem-Name");

    section.addQuestionnaireEntity(qfddMultipleChoiceQuestion);

    qfddDocument.addSection(section);
  }

  private static void addNumeric(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("74465-6", "Continua-Q-QID", "Some-Display-Name", "Some-CodeSystem-Name");
    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDQuestion qfddQuestion = new QFDDNumericQuestion.QFDDNumericQuestionBuilder()
        .setInterval("1", "1", IntervalType.IVL_INT)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Skriv årstal - skriv 9-taller hvis du ikke husker dette")
        .build();

    section.addQuestionnaireEntity(qfddQuestion);

    // Help Text pattern
    QFDDHelpText qfddHelpText = new QFDDHelpText.QFDDHelpTextBuilder()
        .language("da-DK")
        .helpText("Indtast et tal mellem 0 og 24")
        .build();

    // Feedback pattern
    QFDDFeedback qfddFeedback = new QFDDFeedback.QFDDFeedbackBuilder()
        .feedBackText("Don´t take coffeee just before going to bed")
        .build();
    QFDDPrecondition qfddPreconditionFeedback1 = new QFDDPreconditionBuilder(new QFDDCriterionBuilder(codeValue)
        .setMinimum("1")
        .setMaximum("50")
        .setValueType(IntervalType.IVL_INT.name())
        .build()).build();
    QFDDPrecondition qfddPreconditionFeedback2 = new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(codeValue).setAnswer(new CodedValue("A1", "JA")).build()).build();
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback1);
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback2);

    QFDDQuestion qfddQuestion1 = new QFDDNumericQuestion.QFDDNumericQuestionBuilder()
        .setInterval("2", "2", IntervalType.IVL_INT)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Skriv måned - skriv 9-taller hvis du ikke husker dette")
        // .setQFDDMedia(qfddMedia)
        .setHelpText(qfddHelpText)
        .setFeedback(qfddFeedback)
        .build();

    // Example numric precondition
    QFDDPrecondition qfddPrecondition1 = new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(codeValue).setAnswer(new CodedValue("A1", "JA")).build()).build();

    // Example precondition
    QFDDPrecondition qfddPrecondition2 = new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(codeValue).setAnswer(new CodedValue("A1", "JA")).build()).build();

    qfddQuestion1.addPrecondition(qfddPrecondition1);
    qfddQuestion1.addPrecondition(qfddPrecondition2);

    section.addQuestionnaireEntity(qfddQuestion1);

    qfddDocument.addSection(section);
  }

  private static void addText(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("value1", "value2", "value3", "value4");
    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDQuestion qfddQuestion = new QFDDTextQuestion.QFDDTextQuestionBuilder()
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Question")
        .build();

    section.addQuestionnaireEntity(qfddQuestion);

    qfddDocument.addSection(section);
  }

  private static void addQuestionWithComplexPreconditions(QFDDDocument qfddDocument) {
    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("q4", "1.2.208.184.100.2", "displayName", "codeSystemName");

    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = new QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder()
        .setInterval(1, 2)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Har du mere åndenød hoste eller slim? (Ja/Nej)")
        .build();

    qfddMultipleChoiceQuestion.addAnswerOption("A1", "Some-ChoiceDomain-OID", "Extremely Limited",
        "Some-CodeSystem-Name");
    qfddMultipleChoiceQuestion.addAnswerOption("A2", "Some-ChoiceDomain-OID", "Quite a bit Limited",
        "Some-CodeSystem-Name");
    qfddMultipleChoiceQuestion.addAnswerOption("A3", "Some-ChoiceDomain-OID", "Moderately Limited",
        "Some-CodeSystem-Name");

    // Feedback pattern
    QFDDFeedback qfddFeedback = new QFDDFeedback.QFDDFeedbackBuilder()
        .feedBackText("Don´t take coffeee just before going to bed")
        .build();
    QFDDPrecondition qfddPreconditionFeedback1 = new QFDDPreconditionBuilder(new QFDDCriterionBuilder(codeValue)
        .setMinimum("1")
        .setMaximum("50")
        .setValueType(IntervalType.IVL_INT.name())
        .build()).build();
    QFDDPrecondition qfddPreconditionFeedback2 = new QFDDPreconditionBuilder(
        new QFDDCriterionBuilder(codeValue).setAnswer(new CodedValue("A1", "JA")).build()).build();
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback1);
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback2);
    qfddFeedback.addQFDDPrecondition(simplePrecondition());
    qfddFeedback.addQFDDPrecondition(groupedPrecondition());
    qfddFeedback.addQFDDPrecondition(preconditionWithMultipleGroups());

    qfddMultipleChoiceQuestion.addPrecondition(simplePrecondition());
    qfddMultipleChoiceQuestion.addPrecondition(groupedPrecondition());
    qfddMultipleChoiceQuestion.addPrecondition(preconditionWithMultipleGroups());

    section.addQuestionnaireEntity(qfddMultipleChoiceQuestion);

    qfddDocument.addSection(section);
  }

  private static void addCopyRight(QFDDDocument qfddDocument) {
    qfddDocument.setCopyRight(new SectionInformation("Copyright section", "Copyright tekst skrives her"));
  }

  private static void addFinalComment(QFDDDocument qfddDocument) {
    qfddDocument.addSection(new Section<QFDDQuestion>(new SectionInformation("final comment", "final comment")));
  }

  protected static QFDDPrecondition simplePrecondition() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    return new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
        .setMaximum("20")
        .setMinimum("10")
        .setValueType(IntervalType.IVL_INT)
        .build()).build();
  }

  protected static QFDDPrecondition groupedPrecondition() {
    CodedValue questionCodedValue = new CodedValue("value1", "value2", "value3", "value4");
    CodedValue conjunctionCode = new CodedValue("con1", "con2", "con3", "con4");
    return new QFDDPreconditionBuilder(new QFDDPreconditionGroupBuilder()
        .setGroupType(GroupType.AtLeastOneTrue)
        .setId(MedCom.createId("simplePrecondition"))
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("20")
            .setMinimum("10")
            .setValueType(IntervalType.IVL_INT)
            .build()).setConjunctionCode(conjunctionCode).setNegationInd(true).build())
        .addPrecondition(new QFDDPreconditionBuilder(new QFDDCriterionBuilder(questionCodedValue)
            .setMaximum("30")
            .setMinimum("20")
            .setValueType(IntervalType.IVL_INT)
            .build()).setConjunctionCode(conjunctionCode).setNegationInd(true).build())
        .addPrecondition(new QFDDPreconditionBuilder(
            new QFDDCriterionBuilder(questionCodedValue).setAnswer(questionCodedValue).build()).build())
        .build()).build();
  }

  protected static QFDDPrecondition preconditionWithMultipleGroups() {
    return new QFDDPreconditionBuilder(new QFDDPreconditionGroupBuilder()
        .setGroupType(GroupType.AllTrue)
        .setId(MedCom.createId("veryComplexPrecondition"))
        .addPrecondition(groupedPrecondition())
        .addPrecondition(groupedPrecondition())
        .build()).build();
  }
}
