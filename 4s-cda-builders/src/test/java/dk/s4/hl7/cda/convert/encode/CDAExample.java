package dk.s4.hl7.cda.convert.encode;

import dk.s4.hl7.cda.model.core.ClinicalDocument;

public interface CDAExample<E extends ClinicalDocument> {
  E createDocument() throws Exception;
}
