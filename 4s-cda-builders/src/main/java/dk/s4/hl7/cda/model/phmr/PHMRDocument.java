package dk.s4.hl7.cda.model.phmr;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

/**
 * An implementation of SimpleClinicalDocument that only supports the data of
 * the Danish PHMR.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class PHMRDocument extends BaseClinicalDocument {
  private List<MedicalEquipment> medicalEquipmentList;
  private List<Measurement> vitalSignList;
  private List<Measurement> resultList;
  private String medicalEquipmentText;
  private String vitalSignText;
  private String resultText;

  public PHMRDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.PHMR_CODE, Loinc.PMHR_DISPLAYNAME,
        new String[] { MedCom.PHMR_ROOT_POD, MedCom.DK_PHMR_ROOT_POD }, HL7.REALM_CODE_DK);
    this.medicalEquipmentList = new ArrayList<MedicalEquipment>();
    this.vitalSignList = new ArrayList<Measurement>();
    this.resultList = new ArrayList<Measurement>();
    this.medicalEquipmentText = "Medical Equipment";
    this.vitalSignText = "Vital Signs";
    this.resultText = "Results";
  }

  public void addResult(Measurement aMeasurement) {
    resultList.add(aMeasurement);
  }

  public void addVitalSign(Measurement aMeasurement) {
    vitalSignList.add(aMeasurement);
  }

  public void addMedicalEquipment(MedicalEquipment equipment) {
    medicalEquipmentList.add(equipment);
  }

  public List<MedicalEquipment> getMedicalEquipments() {
    return medicalEquipmentList;
  }

  public List<Measurement> getResults() {
    return resultList;
  }

  public List<Measurement> getVitalSigns() {
    return vitalSignList;
  }

  public void setMedicalEquipmentsText(String medicalEquipmentText) {
    this.medicalEquipmentText = medicalEquipmentText;
  }

  public void setResultsText(String resultText) {
    this.resultText = resultText;
  }

  public void setVitalSignsText(String vitalSignText) {
    this.vitalSignText = vitalSignText;
  }

  public String getMedicalEquipmentsText() {
    return medicalEquipmentText;
  }

  public String getResultsText() {
    return resultText;
  }

  public String getVitalSignsText() {
    return vitalSignText;
  }
}
