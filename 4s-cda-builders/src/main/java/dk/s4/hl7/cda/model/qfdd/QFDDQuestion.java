package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.QuestionnaireEntity;

/**
 * Base QFDD question which may hold feedback, help text and preconditions which
 * are common for the all QFDD question types
 */
public class QFDDQuestion extends QuestionnaireEntity {
  private List<QFDDPrecondition> preconditions;
  private QFDDHelpText helpText;
  private QFDDFeedback feedback;

  /**
   * "Effective Java" Builder for constructing QFDDQuestion.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static abstract class BaseQFDDQuestionBuilder<R extends QFDDQuestion, T extends QuestionnaireEntityBuilder<R, T>>
      extends QuestionnaireEntityBuilder<R, T> {
    public BaseQFDDQuestionBuilder() {
      this.preconditions = new ArrayList<QFDDPrecondition>();
    }

    private QFDDHelpText helpText;
    private QFDDFeedback feedback;
    private List<QFDDPrecondition> preconditions;

    public T setHelpText(QFDDHelpText qfddHelpText) {
      this.helpText = qfddHelpText;
      return getThis();
    }

    public T setFeedback(QFDDFeedback qfddFeedback) {
      this.feedback = qfddFeedback;
      return getThis();
    }

    public T addPrecondition(QFDDPrecondition precondition) {
      preconditions.add(precondition);
      return getThis();
    }

    public abstract T getThis();

    public abstract R build();

  }

  protected QFDDQuestion(BaseQFDDQuestionBuilder<?, ?> builder) {
    super(builder);
    helpText = builder.helpText;
    feedback = builder.feedback;
    preconditions = builder.preconditions;
  }

  public QFDDHelpText getHelpText() {
    return helpText;
  }

  public QFDDFeedback getFeedback() {
    return feedback;
  }

  public void addPrecondition(QFDDPrecondition precondition) {
    preconditions.add(precondition);
  }

  public List<QFDDPrecondition> getPreconditions() {
    return preconditions;
  }
}